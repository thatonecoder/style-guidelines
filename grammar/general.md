## Introduction
Speaking in English is a rather difficult job for some. This part of the guide gives some opinionated guidelines for you to follow, making the speech more robust. Since it's quite a heartache to navigate through the entire file, I have decided to provide a — not quite — useful Table of Contents below.

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#be-creative">Don't think, just write</a>
      <ul>
        <li><a href="#widen-your-vocabulary">Widen your vocabulary</a></li>
        <li><a href="#revise">Revise, revise, revise!</a></li>
      </ul>
    </li>
    <li>
      <a href="#how-to-word-like">How to use the word “like”</a>
      <ul>
        <li><a href="#avoid-using-as-filler">Avoid using it as a filler</a></li>
        <li><a href="#like-replacements">Replacements</a></li>
      </ul>
      <ul>
    </li>
  </ol>
</details>

<a name="be-creative"></a>
### Don't think, just write
Since most tend to forget, it is paramount to write everything down, without thinking too much at the beginning — don't worry, you can double-check later. This allows you to have more elaborate writing, as opposed to if you tried to make your writing perfect right away.

<a name="widen-your-vocabulary"></a>
#### Widen your vocabulary
This is the cold truth; most people have a very stale vocabulary, making them seem like idiots — even if they aren't so. Therefore, it's needed for you to continuously train yourself, in order to pass off as a good writer.

<a name="revise"></a>
#### Revise, revise, revise!
Most people post without double-checking — but why is this harmful? Well, it's actually quite simple! When you are writing, you are mostly putting all of your thoughts on paper — or, in this case, a screen! —, your brain is focused on grasping the concept in a way that it is easy to you; however, it doesn't focus on grammar too much, making it essential to revise later on. Revising is the key to making your writing pefect — I myself have revised this text multiple times, in order to make it legible.


<a name="how-to-word-like"></a>
### How to use the word “like”
The word “like” can be useful in certain contexts; but if overused, can make you sound less intelligent, *even* if you are an expert at a theme. This section gives you replacements, among other things (for some, you should use them instead, but others are your choice) that improve your language skills.

<a name="avoid-using-as-filler"></a>
#### Avoid using it as a filler
OK, so, like, most people like, use it in this way like, a lot, right?

*Phew*, you *really* ought not to use it like that. That's the largest usage of the word “like”, and by far the worst offender. Many people use it in their writing, similar to “you know”, “right”, etc — but you could simply remove all the “likes”, and make the sentence somewhat readable.

<a name="like-replacements"></a>
#### Replacements

| With “like” | Replacement |
| ----- | ----- |
| I was(,) like (saying) | I said / yelled / whispered (depends on context) |
