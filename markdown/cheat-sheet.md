## Cheat Sheet
Are you too lazy to look for a specific part to copy? If so, then this is a cheat sheet that will make your day!

### Basic Syntax

Every Markdown app supports these, don't worry.

<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th>Element</th>
      <th>Markdown Syntax</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="#headings">Heading</a></td>
      <td><code># H1</code><br>
      <code>## H2</code><br>
      <code>### H3</code></td>
    </tr>
    <tr>
      <td><a href="#bold">Bold</a></td>
      <td><code>**bold text**</code></td>
    </tr>
    <tr>
      <td><a href="#italic">Italic</a></td>
      <td><code>*italicized text*</code></td>
    </tr>
    <tr>
      <td><a href="#blockquotes-1">Blockquote</a></td>
      <td><code>> blockquote</code></td>
    </tr>
    <tr>
      <td><a href="#ordered-lists">Ordered List</a></td>
      <td><code> 1. First item</code><br>
      <code> 2. Second item</code><br>
      <code> 3. Third item</code></td>
    </tr>
    <tr>
      <td><a href="#unordered-lists">Unordered List</a></td>
      <td><code> - First item</code><br>
      <code> - Second item</code><br>
      <code> - Third item</code></td>
    </tr>
    <tr>
      <td><a href="#code">Code</a></td>
      <td><code>`code`</code></td>
    </tr>
    <tr>
      <td><a href="#horizontal-rules">Horizontal Rule</a></td>
      <td><code>---</code></td>
    </tr>
    <tr>
      <td><a href="#links">Link</a></td>
      <td><code>[title](https://www.example.com)</code></td>
    </tr>
    <tr>
      <td><a href="#images-1">Image</a></td>
      <td><code>![alt text](image.jpg)</code></td>
    </tr>
  </tbody>
</table>

### Advanced Syntax

Not all of the apps that use Markdown support this, but don't worry! Codeberg thankfully supports most.

<table class="table table-bordered">
  <thead class="thead-light">
    <tr>
      <th>Element</th>
      <th>Markdown Syntax</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="/#tables">Table</a></td>
      <td><code>| Syntax      | Description |</code><br>
      <code>| ----------- | ----------- |</code><br>
      <code>| Header      | Title       |</code><br>
      <code>| Paragraph   | Text        |</code></td>
    </tr>
    <tr>
      <td><a href="#fenced-code-blocks">Fenced Code Block</a></td>
      <td><code>```<br>
      {<br>
      &nbsp;&nbsp;"firstName": "John",<br>
      &nbsp;&nbsp;"lastName": "Smith",<br>
      &nbsp;&nbsp;"age": 25<br>
      }<br>
      ```</code></td>
    </tr>
    <tr>
      <td><a href="#footnotes">Footnote</a></td>
      <td><code>
        Here's a sentence with a footnote. [^1]<br><br>
        [^1]: This is the footnote.
      </code></td>
    </tr>
    <tr>
      <td><a href="#heading-ids">Heading ID</a></td>
      <td><code>### My Great Heading {#custom-id}</code></td>
    </tr>
    <tr>
      <td><a href="#definition-lists">Definition List</a></td>
      <td><code>
        term<br>
        : definition
      </code></td>
    </tr>
    <tr>
      <td><a href="#strikethrough">Strikethrough</a></td>
      <td><code>~~The world is flat.~~</code></td>
    </tr>
    <tr>
      <td><a href="#task-lists">Task List</a></td>
      <td><code> - [x] Finished task</code><br>
      <code> - [ ] Unfinished task</code></td>
    </tr>
    <tr>
      <td><a href="#emoji">Emoji</a><br>(see also <a href="/extended-syntax/#copying-and-pasting-emoji">Copying and Pasting Emoji</a>)</td>
      <td><code>
        That is so funny! :joy:
      </code></td>
    </tr>
    <tr>
      <td><a href="#highlight">Highlight</a></td>
      <td><code>I need to highlight these ==very important words==.</code></td>
    </tr>
    <tr>
      <td><a href="#subscript">Subscript</a></td>
      <td><code>H~2~O</code></td>
    </tr>
    <tr>
      <td><a href="#superscript">Superscript</a></td>
      <td><code>X^2^</code></td>
    </tr>
  </tbody>
</table>
