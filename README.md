# This repository has been moved. To open the new repository path, click [here](https://codeberg.org/style-standards/style-guidelines).

## style-guidelines

A thought-out style guide for the English language, Markdown, and other things.